# mcmu

A tiny symbolic model checker for CTL and μ-calculus

# Todo

+ [x] CTL
  - [ ] normal form
  - [ ] CTL to μ-calculus conversion
+ [x] μ-calculus
  - [ ] normal form
  - [ ] syntactic monotonicity checking
  - [ ] nested fixpoints optimization
